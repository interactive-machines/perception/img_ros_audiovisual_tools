IMG ROS Audiovisual Tools
===================

Tools for dealing with audiovisual data in ROS.

Dependencies
------------

The ROS packages in this repository have been tested with [ROS Melodic](http://wiki.ros.org/melodic/Installation) in Ubuntu 18.04. The packages depend on the following libraries:

```bash
$ sudo apt install libgstreamer1.0-0 libgstreamer1.0-dev libgstreamer-plugins-base1.0-0 \
libgstreamer-plugins-base1.0-dev gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly \
ros-melodic-audio-common-msgs ffmpeg
```

### Compiling the packages in a new catkin workspace

Create the catkin workspace once the dependencies are installed:
```bash
$ source /opt/ros/melodic/setup.bash
$ mkdir -p catkin_workspace
$ cd catkin_workspace
$ mkdir -p src
$ cd src
$ catkin_init_workspace
```

Download the source code from the git repository:
```bash
$ git clone https://gitlab.com/interactive-machines/perception/img_ros_audiovisual_tools.git
```

Compile workspace with catkin:
```bash
$ cd ..
$ catkin_make -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_FLAGS=-O3 -DCMAKE_CXX_FLAGS=-O3
$ source devel/setup.bash
```

Quick Start
-----------

So far, the code supports exporting:

- [sensor_msgs/CompressedImage](http://docs.ros.org/melodic/api/sensor_msgs/html/msg/CompressedImage.html) images to a video (compressed with H264 or MJPG codecs)
- [audio_common_msgs/AudioData](http://docs.ros.org/melodic/api/audio_common_msgs/html/msg/AudioData.html) audio to a wav file

### 1. Export video to avi file 

```bash
$ rosrun img_bag_video export_video_from_bag -b <bag_1> <bag_2> ... <bag_N> --image_topic <image_topic> -o video.avi
```

### 2. Export audio to wav file

```bash
$ rosrun img_audio_convert bag2wav -b <bag_1> <bag_2> ... <bag_N> --input-audio-topic=<audio_topic> --output=audio.wav
```

### 3. Export video, audio, and combine into one movie

```bash
$ rosrun img_bag_video make_movie_from_bag.sh <output_prefix> <image_topic> <audio_topic> <bag_1> <bag_2> ... <bag_N>
```

where output_prefix is the name of the final output movie without the extension.

ROS Packages
------------

- **[img_bag_video](img_bag_video)**: Tools to process ROS bags and make videos from image topics.
- **[img_audio_convert](img_audio_convert)**: Tools to export [audio_common_msgs/AudioData](http://docs.ros.org/melodic/api/audio_common_msgs/html/msg/AudioData.html) to wav files.

