IMG Bag Video Package
=====================

Process ROS bags to make videos from image topics.

### Supported Image Formats

- [CompressedImage](http://docs.ros.org/melodic/api/sensor_msgs/html/msg/CompressedImage.html)


### Quick Start

#### Export images into a video (without audio)

Run the `export_video_from_bag` tool:

```bash
$ rosrun img_bag_video export_video_from_bag -b <bag1> <bag2> ... --image_topic <image_topic>
```

By default, the tool will generate a video named `output.avi` with images from the given topic at 15 fps.
Use the following additional arguments to customize operation:

- **output:** (string) name of the output file including extension (only .avi is supported)
- **fps:** (int) frame rate of the output video
- **codec:** (string) codec for the output video (only 'H264' -- which is the default -- or 'MJPG' are supported)

#### Export images and audio into a video

Run the `make_movie_from_bag.sh` script as shown below. The script will use the `export_video_from_bag` tool above and the `img_audio_convert` package to export images and audio. Finally, the script will combine the generated files using [ffmpeg](https://ffmpeg.org/).

```bash
$ rosrun img_bag_video make_movie_from_bag.sh <output_prefix> <image_topic> <audio_topic> <bag_1> <bag_2> ... <bag_N>
```

By default, the script exports the video at 30 fps using the H264 codec. The output movie is named <output_prefix>.mp4.