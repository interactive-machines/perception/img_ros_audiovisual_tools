// This file is similar to image_view/src/nodes/video_recorder.cpp but instead of getting images through ROS, it loads
// them up from a ROS bag that is passed as input to it.

/****************************************************************************
* Software License Agreement (Apache License)
*
*     Copyright (C) 2012-2013 Open Source Robotics Foundation
*
*     Licensed under the Apache License, Version 2.0 (the "License");
*     you may not use this file except in compliance with the License.
*     You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
*     Unless required by applicable law or agreed to in writing, software
*     distributed under the License is distributed on an "AS IS" BASIS,
*     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*     See the License for the specific language governing permissions and
*     limitations under the License.
*
*****************************************************************************/

#include <opencv2/highgui/highgui.hpp>
#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <iostream>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <camera_calibration_parsers/parse.h>
#if CV_MAJOR_VERSION == 3
#include <opencv2/videoio.hpp>
#endif
#include <boost/program_options.hpp>
#include <deque>

namespace po = boost::program_options;

cv::VideoWriter outputVideo;

int g_count = 0;
ros::Time g_last_wrote_time = ros::Time(0);
std::string encoding;
std::string codec;
int fps;
std::string filename;
double min_depth_range;
double max_depth_range;
bool use_dynamic_range;
int colormap;
int max_deque_length = 300;

std::deque<sensor_msgs::ImageConstPtr> image_queue;
sensor_msgs::ImageConstPtr last_added_image_ptr;

namespace enc = sensor_msgs::image_encodings;

/**
 * Helper function to convert a compressed image into a pointer for an image message
 * @param message compressed image
 * @param imdecode_flag_ opencv imdecode flag (see https://docs.opencv.org/3.1.0/d4/da8/group__imgcodecs.html)
 * @return pointer to sensor image
 */
sensor_msgs::ImagePtr
compressed_image_to_cv(const sensor_msgs::CompressedImageConstPtr& message, int imdecode_flag_ = cv::IMREAD_COLOR)
{

  std::shared_ptr<cv_bridge::CvImage> cv_ptr = std::shared_ptr<cv_bridge::CvImage>(new cv_bridge::CvImage);

  // Copy message header
  cv_ptr->header = message->header;

  // Decode color/mono image
  try
  {
    cv_ptr->image = cv::imdecode(cv::Mat(message->data), imdecode_flag_);

    // Assign image encoding string
    const size_t split_pos = message->format.find(';');
    if (split_pos==std::string::npos)
    {
      // Older version of compressed_image_transport does not signal image format
      switch (cv_ptr->image.channels())
      {
        case 1:
          cv_ptr->encoding = enc::MONO8;
          break;
        case 3:
          cv_ptr->encoding = enc::BGR8;
          break;
        default:
          ROS_ERROR("Unsupported number of channels: %i", cv_ptr->image.channels());
          break;
      }
    } else
    {
      std::string image_encoding = message->format.substr(0, split_pos);

      cv_ptr->encoding = image_encoding;

      if ( enc::isColor(image_encoding))
      {
        std::string compressed_encoding = message->format.substr(split_pos);
        bool compressed_bgr_image = (compressed_encoding.find("compressed bgr") != std::string::npos);

        // Revert color transformation
        if (compressed_bgr_image)
        {
          // if necessary convert colors from bgr to rgb
          if ((image_encoding == enc::RGB8) || (image_encoding == enc::RGB16))
            cv::cvtColor(cv_ptr->image, cv_ptr->image, CV_BGR2RGB);

          if ((image_encoding == enc::RGBA8) || (image_encoding == enc::RGBA16))
            cv::cvtColor(cv_ptr->image, cv_ptr->image, CV_BGR2RGBA);

          if ((image_encoding == enc::BGRA8) || (image_encoding == enc::BGRA16))
            cv::cvtColor(cv_ptr->image, cv_ptr->image, CV_BGR2BGRA);
        } else
        {
          // if necessary convert colors from rgb to bgr
          if ((image_encoding == enc::BGR8) || (image_encoding == enc::BGR16))
            cv::cvtColor(cv_ptr->image, cv_ptr->image, CV_RGB2BGR);

          if ((image_encoding == enc::BGRA8) || (image_encoding == enc::BGRA16))
            cv::cvtColor(cv_ptr->image, cv_ptr->image, CV_RGB2BGRA);

          if ((image_encoding == enc::RGBA8) || (image_encoding == enc::RGBA16))
            cv::cvtColor(cv_ptr->image, cv_ptr->image, CV_RGB2RGBA);
        }
      }
    }
  }
  catch (cv::Exception& e)
  {
    ROS_ERROR("%s", e.what());
  }

  size_t rows = cv_ptr->image.rows;
  size_t cols = cv_ptr->image.cols;

  if ((rows > 0) && (cols > 0))
    // Publish message to user callback

  return cv_ptr->toImageMsg();
}

/**
 * Helper function for limiting the length of the deque used to store image frames when building the video
 */
void clean_up_queue()
{
    std::deque<sensor_msgs::ImageConstPtr>::iterator it_ref = image_queue.end(); // no image should be added by default
    for (std::deque<sensor_msgs::ImageConstPtr>::iterator it = image_queue.begin(); it != image_queue.end(); ++it)
    {
        sensor_msgs::ImageConstPtr img = *it;
        if (img->header.stamp <= g_last_wrote_time - ros::Duration(1.0/float(fps))) {
            it_ref = it;
        } else {
            break;
        }
    }
    if (it_ref != image_queue.end()) {
        image_queue.erase(image_queue.begin(), it_ref);
    }

    if (image_queue.size() > max_deque_length) {
        std::cout << "Warning. removing frames from the queue. Is the framerate really low? Something might be wrong."
                  << std::endl;
        int num_remove = image_queue.size() - max_deque_length;
        image_queue.erase(image_queue.begin(), image_queue.begin() + num_remove);
    }
}

/**
 * Helper function to add image to video
 * @param image_ptr pointer to image message
 */
void
add_image_to_video(const sensor_msgs::ImageConstPtr& image_ptr, bool first_frame)
{
    try
    {
        cv_bridge::CvtColorForDisplayOptions options;
        options.do_dynamic_scaling = use_dynamic_range;
        options.min_image_value = min_depth_range;
        options.max_image_value = max_depth_range;
        options.colormap = colormap;
        const cv::Mat image = cv_bridge::cvtColorForDisplay(cv_bridge::toCvShare(image_ptr), encoding, options)->image;
        if (!image.empty()) {
            outputVideo << image;

            g_count++;
            if (g_count % 10 == 0)
                ROS_INFO_STREAM("Recording frame " << g_count << " (stamp: " << std::fixed << image_ptr->header.stamp.toSec() << ")."
                                << " Dequeue size = " << image_queue.size() << "/" << max_deque_length << ". \x1b[1F");

        } else {
            ROS_WARN("Frame skipped, no data!");
        }
    } catch(cv_bridge::Exception)
    {
        ROS_ERROR("Unable to convert %s image to %s", image_ptr->encoding.c_str(), encoding.c_str());
        return;
    }

    // save last image and update last stamp
    last_added_image_ptr = image_ptr;
    if (first_frame)
    {
        g_last_wrote_time = image_ptr->header.stamp;
    }
    else
    {
        g_last_wrote_time = g_last_wrote_time + ros::Duration(1.0 / float(fps));
    }
}

void fill_in_image(const sensor_msgs::ImageConstPtr& image, const ros::Time& next_stamp)
{

    ros::Time img_stamp = image->header.stamp;

    // add as many images as necessary until we reach the right stamp
    while (g_last_wrote_time - next_stamp <= ros::Duration(0)) {

        sensor_msgs::Image filler_img = *image;
        sensor_msgs::ImagePtr filler_img_ptr = boost::make_shared<sensor_msgs::Image>(filler_img);

        add_image_to_video(filler_img_ptr, false);
    }

}

/**
 * Helper function to process and image and add it to the queue and video if appropriate based on the timestamp
 * @param image_msg pointer to image message
 */
void process_image(const sensor_msgs::ImageConstPtr& image_msg)
{
    if (!outputVideo.isOpened()) {

        cv::Size size(image_msg->width, image_msg->height);

        outputVideo.open(filename,
#if CV_MAJOR_VERSION >= 3
                cv::VideoWriter::fourcc(codec.c_str()[0],
#else
                CV_FOURCC(codec.c_str()[0],
#endif
                          codec.c_str()[1],
                          codec.c_str()[2],
                          codec.c_str()[3]),
                fps,
                size,
                true);

        if (!outputVideo.isOpened())
        {
            ROS_ERROR("Could not create the output video! Check filename and/or support for codec.");
            exit(-1);
        }

        ROS_INFO_STREAM("Starting to record " << codec << " video at " << size << "@" << fps << "fps. Press Ctrl+C to stop recording." );

    }

    // put into the queue
    image_queue.push_back(image_msg);

    // first frame?
    bool first_frame = (g_last_wrote_time == ros::Time(0));
    ros::Time next_stamp = g_last_wrote_time + ros::Duration(1.0 / float(fps));

    // process queue. Find the index of the image that should be added to the video.
    std::deque<sensor_msgs::ImageConstPtr>::iterator it_ref = image_queue.end(); // no image should be added by default
    for (std::deque<sensor_msgs::ImageConstPtr>::iterator it = image_queue.begin(); it != image_queue.end(); ++it)
    {
        sensor_msgs::ImageConstPtr image_ptr = *it;
        if (first_frame) {
            // first frame in the video. just use it!
            it_ref = it;
            break;
        }

        ros::Duration time_diff = image_ptr->header.stamp - next_stamp;
        if (time_diff > ros::Duration(0)) {
            // passed the current stamp. so we use the previous image
            it_ref = it;
            break;
        }
    }

    // get image pointer
    sensor_msgs::ImageConstPtr image_ptr_add;
    if (first_frame) {
        image_ptr_add = *it_ref;
        add_image_to_video(image_ptr_add, first_frame);
        return;

    } else if (it_ref == image_queue.end()) {

        if (g_last_wrote_time < (*(image_queue.begin()))->header.stamp) {
            // fill in video with black frames
            add_image_to_video(last_added_image_ptr, first_frame);
        }

        clean_up_queue();
        return;
    }
    else if (it_ref == image_queue.begin()) {
        image_ptr_add = last_added_image_ptr;
        add_image_to_video(image_ptr_add, first_frame);
        return;

    }
    else {

        image_ptr_add = *(it_ref - 1); // add prior one
        fill_in_image(image_ptr_add, (*it_ref)->header.stamp);

//        // add prior image as many times as necessary until we reach the stamp of the next image in the queue
//        while (g_last_wrote_time - ((*it_ref)->header.stamp - ros::Duration(1.0 / float(fps))) <= ros::Duration(0)) {
//            add_image_to_video(image_ptr_add, false);
//        }


    }

    // add image to video
//    add_image_to_video(image_ptr_add, first_frame);

    // remove old elements from queue
//    if (it_ref != image_queue.begin()) image_queue.erase(image_queue.begin(), it_ref + 1);
    clean_up_queue();

}

/**
 * Helper function to get the start time of the audio topic (used as the start time of the movie if requested by the user)
 * @param audio_topic audio topic
 * @param bags_vector organized list of input bags
 * @return time of the first audio message recorded in the audio topic
 */
ros::Time
get_audio_stamp(std::string& audio_topic, std::vector< std::tuple< std::string, ros::Time > >& bags_vector)
{
    std::vector<std::string> audio_topics;
    audio_topics.push_back(audio_topic);

    ros::Time audio_stamp = ros::Time(0);
    for (auto it = std::begin(bags_vector); it!=std::end(bags_vector); ++it){

        std::string bag_file = std::get<0>(*it);

        rosbag::Bag bag;
        bag.open(bag_file, rosbag::bagmode::Read);

        rosbag::View view(bag, rosbag::TopicQuery(audio_topics));

        for(rosbag::MessageInstance const m : view)
        {
            audio_stamp = m.getTime();
            break;
        }

        bag.close();

        if (audio_stamp != ros::Time(0)) {
            break;
        }
    }
    if (audio_stamp == ros::Time(0)) {
        ROS_WARN_STREAM("Failed to get a start stamp from the audio topic " << audio_topic << ". Check the topic name.");
    }
    return audio_stamp;
}

/**
 * Main function
 * @param argc number of command line arguments
 * @param argv command line arguments
 * @return
 */
int main(int argc, char** argv)
{

    // params
    encoding = std::string("bgr8");
    colormap = -1;
    std::string image_topic;
    std::vector<std::string> bag_files;
    double start_time = 0.0;
    std::string audio_topic;

    // Declare the supported options.
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("bags,b", po::value< std::vector<std::string> >(&bag_files)->required()->multitoken(), "input bags")
        ("image_topic", po::value<std::string>(&image_topic)->required(), "image topic")
        ("audio_topic", po::value<std::string>(&audio_topic)->default_value(""), "audio topic (if not empty, the time of the first audio message will be used for the start of the video)")
        ("output,o", po::value<std::string>(&filename)->default_value(std::string("output.avi")), "output name")
        ("codec,c", po::value<std::string>(&codec)->default_value(std::string("H264")), "codec (e.g., 'MJPG', 'H264')")
        ("fps", po::value<int>(&fps)->default_value(15), "frame rate of the output video")
        ("start_time", po::value<double>(&start_time)->default_value(0.0), "start time (in seconds) for the video. Set to 0 for the time of the first image. Overrides audio_topic.")
    ;

    po::positional_options_description p;
    p.add("bags", -1);
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << "\n";
        return 1;
    }

    if (vm.count("bags") == 0) {
        ROS_ERROR("Please provide at least one input file. For example: rosrun img_bag_video make_bag_from_video --image_topic <image_topic> <input_bag>");
        return 1;
    }

    // organize bags based on the stamps in the image topic
    std::vector< std::tuple< std::string, ros::Time > > bags_vector;

    std::cout << "Image topic: " << image_topic << std::endl;
    std::vector<std::string> topics;
    topics.push_back(image_topic);

    // are the images compressed? decide based on the topic name
    std::string compressed_name = "compressed";
    int L = image_topic.length();
    std::string suffix = (L > compressed_name.length()) ? image_topic.substr(L - compressed_name.length()) : "";
    bool is_compressed = suffix.compare(compressed_name) == 0 ? true : false;

    // first get start times so that we can sort the input bags
    for (auto it = std::begin(bag_files); it!=std::end(bag_files); ++it){

        std::string bag_file = *it;

        rosbag::Bag bag;
        bag.open(bag_file, rosbag::bagmode::Read);

        rosbag::View view(bag, rosbag::TopicQuery(topics));

        for(rosbag::MessageInstance const m : view)
        {
             ros::Time stamp;
             if (is_compressed) {
                sensor_msgs::CompressedImage::ConstPtr img_ptr = m.instantiate<sensor_msgs::CompressedImage>();
                stamp = img_ptr->header.stamp;
             } else {
                sensor_msgs::Image::ConstPtr img_ptr = m.instantiate<sensor_msgs::Image>();
                stamp = img_ptr->header.stamp;
             }

             std::tuple<std::string, ros::Time> tup(bag_file, stamp);
             bags_vector.push_back(tup);

             break;
        }

        bag.close();

    }

    if (bags_vector.size() < 1) {
        std::cout << "The bags do not seem to have the topic " << image_topic << ". Check the files." << std::endl;
        exit(-1);
    }

    // sort the inputs
    std::sort(std::begin(bags_vector), std::end(bags_vector), [](auto const &t1, auto const &t2) {
        return std::get<1>(t1) < std::get<1>(t2); // or use a custom compare function
    });

    // check when the audio starts
    if (audio_topic.length() > 0) {
        ros::Time audio_stamp = get_audio_stamp(audio_topic, bags_vector);
        start_time = audio_stamp.toSec();
        std::cout << "Audio topic: " << audio_topic << " (start time: " << start_time << ")" << std::endl;
    }

    // process sorted bags
    std::cout << "Making video with " << bags_vector.size() << " bag(s):" << std::endl;
    for (std::tuple< std::string, ros::Time > tup : bags_vector){
        std::string name = std::get<0>(tup);
        ros::Time stamp = std::get<1>(tup);
        std::cout << "- Processing " << name << " (start time: " << std::fixed << stamp.toSec() << ")" << std::endl;

        rosbag::Bag bag;
        bag.open(name, rosbag::bagmode::Read);

        rosbag::View view(bag, rosbag::TopicQuery(topics));
        ros::Duration fps_duration = ros::Duration(1.0 / float(fps));

        for(rosbag::MessageInstance const m : view)
        {
             sensor_msgs::ImagePtr image;
             ros::Time stamp;

             if (is_compressed) {
                sensor_msgs::CompressedImage::ConstPtr compressed_img_ptr = m.instantiate<sensor_msgs::CompressedImage>();
                image = compressed_image_to_cv(compressed_img_ptr);
                stamp = compressed_img_ptr->header.stamp;
             } else {
                image = m.instantiate<sensor_msgs::Image>();
                stamp = image->header.stamp; // todo. needs testing at this point!
             }

            // add black image if a given start time was requested
            if (start_time > 0.0) {

                // hack to copy frames if necessary into the video container if the start_time is less than the first video stamp
                ros::Time right_stamp = image->header.stamp;
                ros::Time fake_start_time = ros::Time(start_time);

                // add as many images as necessary until we reach the right stamp
                int counter;
                while ((right_stamp - fake_start_time) > fps_duration) {

                    sensor_msgs::Image filler_img = *image;
                    sensor_msgs::ImagePtr filler_img_ptr = boost::make_shared<sensor_msgs::Image>(filler_img);

                    filler_img_ptr->header.stamp = fake_start_time;
                    process_image(filler_img_ptr);
                    fake_start_time = fake_start_time + fps_duration;
                    counter++;
                }
//                std::cout << "Added " << counter << " extra images at the beginning of the video to match start time." << std::endl;
                image->header.stamp = right_stamp;

                start_time = 0.0;

            }

            // finally, process the actual image from the bag
            process_image(image);

        }

        bag.close();
    }

    ROS_INFO_STREAM("Done. Recorded " << g_count << "frames.");

    return 0;
}
