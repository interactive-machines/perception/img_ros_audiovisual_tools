#!/usr/bin/env bash

# default params
FPS=30

# arguments
if [ $# -lt 4 ]; then
    echo "Not enough arguments were supplied"
    echo "Run as: ./make_movie_from_bag.sh <output_prefix> <images_topic> <audio_topic> <bag_file1> <bag_file2> ... <bag_fileN>"
    exit 1
fi

OUTPUT_PREFIX=$1
IMAGES_TOPIC=$2
AUDIO_TOPIC=$3
INPUT_BAG=${@:4}

echo "-- Input options --"
echo "Output prefix: $OUTPUT_PREFIX"
echo "Images topic: $IMAGES_TOPIC"
echo "Audio topic: $AUDIO_TOPIC"
echo "Input bags: ${INPUT_BAG}"
echo "Fps: $FPS"

# create video
echo "-- Exporting video --"
TMP_VIDEO=${OUTPUT_PREFIX}_tmp.avi
if [ -f ${TMP_VIDEO} ]; then
    echo "Skipping generation of video file because ${TMP_VIDEO} already exists."
else
    rosrun img_bag_video export_video_from_bag -b ${INPUT_BAG} \
	   --image_topic ${IMAGES_TOPIC} --fps ${FPS} --audio_topic  ${AUDIO_TOPIC} --output ${TMP_VIDEO}
fi
      
# create wav file
echo "-- Exporting audio --"
TMP_WAV=${OUTPUT_PREFIX}_tmp.wav
if [ -f ${TMP_WAV} ]; then
    echo "Skipping generation of wav file because ${TMP_WAV} already exists."
else
    rosrun img_audio_convert bag2wav -b ${INPUT_BAG} --output=${TMP_WAV} --input-audio-topic=${AUDIO_TOPIC}
fi

# combine audio and video
FINAL_VIDEO=${OUTPUT_PREFIX}.mp4
echo "-- Combining video and audio --"
ffmpeg -i ${TMP_VIDEO} -i ${TMP_WAV} -c copy -c:a aac -b:a 192k ${FINAL_VIDEO}

echo "-- Cleaning up --"
echo "Do you want to delete temporary files (${TMP_VIDEO}, ${TMP_WAV})?"
select yn in "Yes" "No"; do
    case $yn in
	Yes ) rm ${TMP_VIDEO}; rm ${TMP_WAV}; echo "Deleted temp files."; break;;
	No ) echo "Done."; break;;
    esac
done

exit 0
