# IMG Audio Convert (formerly ros-audio-convert)
ROS (Robotic Operating System) packages for audio conversion based on the audio_common stack. This package is a modifed version of [ros-audio-convert](https://github.com/sbrodeur/ros-audio-convert).

## Installing dependencies

Install dependencies (Debian/Ubuntu):
```
sudo apt-get install libgstreamer1.0-0 libgstreamer1.0-dev libgstreamer-plugins-base1.0-0 libgstreamer-plugins-base1.0-dev gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly ros-kinetic-audio-common-msgs
```

## Usage

Convert a single bag file to wav:
```
rosrun img_audio_convert bag2wav -b audio.bag --output=audio.wav --input-audio-topic=/audio
```